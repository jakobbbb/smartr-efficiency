#!/bin/bash

# Script to build Maven Dependencies from source

# List of dependencies to build from source.
# Can point to the root of a repository containing a pom.xml (e.g.
# https://github.com/example/stuff) or to a sub-folder containing a
# pom.xml (e.g. https://github.com/example/stuff/module/)
DEPS=("
    https://github.com/smartrplace/api-draft/src
    https://github.com/ogema/ogema-widgets-extended/src
    ")

build () {
    cd /tmp
    REPO=$(grep -oE "http(s|)://\w+.\w+/[^/]+/[^/]+" <<< $1)
    CLONE_DEST=$(sed -s "s~/~\n~g" <<< $REPO | tail -n 1)
    git clone $REPO $CLONE_DEST
    cd $CLONE_DEST
    SUBFOLDER_TAG=$(sed -e "s~$REPO[/]*~~g" <<< $1)
    SUBFOLDER=$(cut -d@ -f1 <<< $SUBFOLDER_TAG)
    TAG=$(cut -d@ -f2 <<< $SUBFOLDER_TAG)
    cd $SUBFOLDER
    git checkout $TAG
    mvn clean install -DskipTests --fail-at-end
    cd /tmp
    rm -fr $CLONE_DEST
}

STARTDIR=($pwd)
for DEP in $DEPS; do
    echo "\n\n\n\n\n\n\n\n\n\n"
    echo "Building $DEP"
    build $DEP
    cd $STARTDIR
done
